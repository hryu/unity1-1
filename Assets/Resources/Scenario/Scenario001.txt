fg

// 로딩
showloading
wait 0.1

loadbg maobg
loadbgm normal
loadmodel Haru
loadmodel Epsilon

bgm normal
wait 0.1
removeloading
showmenu

name
text [마왕성]

name 용사
text 여기가 마왕성인가..?

bg maobg
fgout 1.0

model Epsilon idle true empty (-0.8,-1,0) 1.0
wait 0.5

expression Epsilon smile
motion Epsilon nod true
name 용사
text 마왕을 무찌르고 세상을 구하겠어!
hidetext

wait 1.0
expression Epsilon surprise
motion Epsilon surprise
name ???
text . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . \n후후후.. 여기까지 도달하다니 대단하구나...
hidetext
expression Epsilon empty

wait 1.0
motion Epsilon angry
name 용사
text 누구냐!! 모습을 보여라!!
hidetext

wait 1.0
//////////////////////////////////////////////// 마왕 등장
bgcolor (255,255,255)
wait 0.05
bg maobg
wait 0.05
bgcolor (255,255,255)
wait 0.05
bg maobg
wait 0.05

sound decide1

motion Epsilon surprise
model Haru idle true empty (0.7,-1,0) 0.1
wait 0.5
motion Haru foldSmile
wait 0.5
motion Haru haru_normal_10
name 마왕
text 겁도 없이 소리치는걸 보니 배짱은 조금 있구나
expression Epsilon angry
motion Haru haru_normal_09
text 내가 바로 이 성의 주인이다
hidetext
expression Epsilon surprise
motion Haru gatherSmile

//////////////////////////////////////////////// 
wait 2.5
motion Haru haru_normal_10
name 마왕
text 자. 너무 그렇게 긴장하지 말고.
motion Haru haru_normal_10
text 후훗.. 100년만에 온 손님이라서 엄청 설레는데 뭐부터 하는게 좋을....
expression Epsilon empty
motion Epsilon angry
name 용사
text (안들림) 이야야아아아아아아앗!!!(선공필승!)
hidetext

//////////////////////////////////////////////// 선택지
label selectStart

select
selectitem attack1 선공필승!!
selectitem attack2 문답무용!!
selectitem attack3 초필살기!!
selectend

label attack1
//////////////////////////////////////////////// 공격1
sound Jjan
bgcolor (255,255,255)
modelpos Epsilon (0.0,-1,0) 0.0
modelpos Haru (0.8,-1,0) 0.0
wait 0.05
bg maobg
wait 0.05
bgcolor (255,255,255)
wait 0.05
bg maobg
wait 0.05
modelpos Epsilon (-0.8,-1,0) 0.0
modelpos Haru (0.7,-1,0) 0.0

motion Haru surprise
motion Epsilon tilt true
expression Epsilon angry

wait 1.5
name 마왕
text !!!!!???????
hidetext

wait 0.5
motion Haru haru_normal_01
name 마왕
text 꽤나 성격급하네
hidetext
wait 0.5
motion Haru haru_normal_09
text 난 오랜만에 인간을 만나는건데 잠깐 얘기라도 하는게 어때?
hidetext

wait 0.5
motion Epsilon idle
expression Epsilon angry
name 용사
text 너와 나눌 얘기 따윈 없다!
hidetext

wait 0.5
name 마왕
motion Haru consider
text 음... 
expression Epsilon surprise
motion Haru gatherWave
text 아! 그러면 1주차 과제 얘기라도.....?
hidetext

wait 0.5
expression Epsilon empty
motion Epsilon angry
name 용사
text (안들림) 이 세상의 평화를 위해 당장 사라져랏!!! (문답무용!)
hidetext

jump selectStart

label attack2
//////////////////////////////////////////////// 공격2
sound Jjan
bgcolor (255,255,255)
modelpos Epsilon (0.0,-1,0) 0.0
modelpos Haru (0.8,-1,0) 0.0
wait 0.05
bg maobg
wait 0.05
bgcolor (255,255,255)
wait 0.05
bg maobg
wait 0.05
modelpos Epsilon (-0.8,-1,0) 0.0
modelpos Haru (0.7,-1,0) 0.0

motion Haru surprise
motion Epsilon tilt true
expression Epsilon angry

wait 1.0
name 마왕
text 아야!! 
expression Haru sad
text 진짜 때렸어??
hidetext

wait 0.5
motion Haru disappoint
motion Epsilon idle
name 용사
text 공격이 안먹히나? 역시 마왕은 튼튼하군
expression Haru surprise
text 우랴아아아아아아ㅏㅇ아ㅏㅏㅏ!!!!! (초필살기!)

jump selectStart

label attack3
//////////////////////////////////////////////// (초필살기) 공격3
expression Haru empty
name 용사
text (기모으는중) 하아앗!! 하아아ㅏ아ㅏㅏㅏ아아아앗!!!!!!!

motion Epsilon shake true
expression Epsilon angry

////////////// 번쩍번쩍 용사 부들부들 마왕 흔들흔들
bgcolor (255,255,255)
wait 0.05
bg maobg
wait 0.15

bgcolor (255,225,225)
modelpos Epsilon (-0.8,-1.02,0) 0.0
modelpos Epsilon (-0.8,-1,0) 0.0
wait 0.05
bg maobg
modelpos Epsilon (-0.8,-1.02,0) 0.0
modelpos Epsilon (-0.8,-1,0) 0.0
wait 0.05
bgcolor (255,255,255)
modelpos Epsilon (-0.8,-1.02,0) 0.0
modelpos Epsilon (-0.8,-1,0) 0.0
wait 0.05
bg maobg
modelpos Epsilon (-0.8,-1.02,0) 0.0
modelpos Epsilon (-0.8,-1,0) 0.0
wait 0.05

motion Haru surprise true

modelpos Epsilon (-0.8,-1.02,0) 0.0
modelpos Epsilon (-0.8,-1,0) 0.0
modelpos Epsilon (-0.8,-1.02,0) 0.0
modelpos Epsilon (-0.8,-1,0) 0.0
modelpos Epsilon (-0.8,-1.02,0) 0.0
modelpos Epsilon (-0.8,-1,0) 0.0
modelpos Epsilon (-0.8,-1.02,0) 0.0
modelpos Epsilon (-0.8,-1,0) 0.0

bgcolor (255,195,195)
modelpos Epsilon (-0.8,-1.02,0) 0.0
modelpos Epsilon (-0.8,-1,0) 0.0
wait 0.05
bg maobg
modelpos Epsilon (-0.8,-1.02,0) 0.0
modelpos Epsilon (-0.8,-1,0) 0.0
wait 0.05
bgcolor (255,225,225)
modelpos Epsilon (-0.8,-1.02,0) 0.0
modelpos Epsilon (-0.8,-1,0) 0.0
wait 0.05
bg maobg
modelpos Epsilon (-0.8,-1.02,0) 0.0
modelpos Epsilon (-0.8,-1,0) 0.0
wait 0.05

modelpos Epsilon (-0.8,-1.02,0) 0.0
modelpos Epsilon (-0.8,-1,0) 0.0
modelpos Epsilon (-0.8,-1.02,0) 0.0
modelpos Epsilon (-0.8,-1,0) 0.0
modelpos Epsilon (-0.8,-1.02,0) 0.0
modelpos Epsilon (-0.8,-1,0) 0.0
modelpos Epsilon (-0.8,-1.02,0) 0.0
modelpos Epsilon (-0.8,-1,0) 0.0

bgcolor (255,165,165)
modelpos Epsilon (-0.8,-1.02,0) 0.0
modelpos Epsilon (-0.8,-1,0) 0.0
wait 0.05
bg maobg
modelpos Epsilon (-0.8,-1.02,0) 0.0
modelpos Epsilon (-0.8,-1,0) 0.0
wait 0.05
bgcolor (255,195,195)
modelpos Epsilon (-0.8,-1.02,0) 0.0
modelpos Epsilon (-0.8,-1,0) 0.0
wait 0.05
bg maobg
modelpos Epsilon (-0.8,-1.02,0) 0.0
modelpos Epsilon (-0.8,-1,0) 0.0
wait 0.05

modelpos Epsilon (-0.8,-1.04,0) 0.0
modelpos Epsilon (-0.8,-1,0) 0.0
modelpos Epsilon (-0.8,-1.04,0) 0.0
modelpos Epsilon (-0.8,-1,0) 0.0
modelpos Epsilon (-0.8,-1.04,0) 0.0
modelpos Epsilon (-0.8,-1,0) 0.0
modelpos Epsilon (-0.8,-1.04,0) 0.0
modelpos Epsilon (-0.8,-1,0) 0.0

bgcolor (255,115,115)
modelpos Epsilon (-0.8,-1.02,0) 0.0
modelpos Epsilon (-0.8,-1,0) 0.0
wait 0.05
bg maobg
modelpos Epsilon (-0.8,-1.02,0) 0.0
modelpos Epsilon (-0.8,-1,0) 0.0
wait 0.05
bgcolor (255,165,165)
modelpos Epsilon (-0.8,-1.02,0) 0.0
modelpos Epsilon (-0.8,-1,0) 0.0
wait 0.05
bg maobg
modelpos Epsilon (-0.8,-1.02,0) 0.0
modelpos Epsilon (-0.8,-1,0) 0.0
wait 0.05

modelpos Epsilon (-0.8,-1.02,0) 0.0
modelpos Epsilon (-0.8,-1,0) 0.0
modelpos Epsilon (-0.8,-1.04,0) 0.0
modelpos Epsilon (-0.8,-1,0) 0.0
modelpos Epsilon (-0.8,-1.02,0) 0.0
modelpos Epsilon (-0.8,-1,0) 0.0
modelpos Epsilon (-0.8,-1.04,0) 0.0
modelpos Epsilon (-0.8,-1,0) 0.0

bgcolor (255,115,115)
modelpos Epsilon (-0.8,-1.02,0) 0.0
modelpos Epsilon (-0.8,-1,0) 0.0
wait 0.05
bg maobg
modelpos Epsilon (-0.8,-1.02,0) 0.0
modelpos Epsilon (-0.8,-1,0) 0.0
wait 0.05
bgcolor (255,125,125)
modelpos Epsilon (-0.8,-1.06,0) 0.0
modelpos Epsilon (-0.8,-1,0) 0.0
wait 0.05
bg maobg
modelpos Epsilon (-0.8,-1.06,0) 0.0
modelpos Epsilon (-0.8,-1,0) 0.0
wait 0.15

modelpos Epsilon (-0.8,-1.06,0) 0.0
modelpos Epsilon (-0.8,-1,0) 0.0
modelpos Epsilon (-0.8,-1.06,0) 0.0
modelpos Epsilon (-0.8,-1,0) 0.0
modelpos Epsilon (-0.8,-1.06,0) 0.0
modelpos Epsilon (-0.8,-1,0) 0.0
modelpos Epsilon (-0.8,-1.06,0) 0.0
modelpos Epsilon (-0.8,-1,0) 0.0

motion Haru surprise
name 마왕
text ?????? 허억!? 잠깐!!!! 잠깐잠깐!!
hidetext

wait 0.5
motion Epsilon idle
expression Epsilon surprise
motion Haru disappoint
name 용사
text ???
text 뭐야??
hidetext

wait 0.5
motion Haru haru_m_10
name 마왕
text 님 너무 몰입한거 아님?? (과제에)
hidetext

wait 0.5
expression Epsilon empty
motion Epsilon angry true
name 용사
text 뭐래? 
hidetext

//////////////////////////////////////////////// 공격
sound Jjan
bgcolor (255,255,255)
modelpos Epsilon (0.0,-1,0) 0.0
modelpos Haru (0.8,-1,0) 0.0
wait 0.02
bg maobg
wait 0.02
bgcolor (255,255,255)
wait 0.02
bg maobg
wait 0.02
modelpos Epsilon (-0.8,-1,0) 0.0
modelpos Haru (0.7,-1,0) 0.0

motion Haru surprise

wait 1.5
name 마왕
text 아야!! 
motion Epsilon what
expression Epsilon surprise
text 자꾸 진짜 때리지 말라고!!
motion Haru haru_m_10 true
expression Epsilon surprise
text 우이쒸이... 말할 때 입도 못움직이는게...
hidetext

wait 0.5
expression Epsilon empty
motion Epsilon doNot
name 용사
text !!!!
hidetext

wait 0.5
motion Haru haru_normal_05 true
name 마왕
text 후후후후후후......
hidetext

expression Epsilon surprise
motion Haru haru_normal_07 true
text 데이터 파일 까보니깐 내가 동작도 훨씬 많고 입도 움직여서\n대사 더 많은 마왕 역을 맡게되었단 사실!
motion Haru haru_normal_03 true
expression Epsilon shy
text 대체 입도 안움직이고 어떻게 말하는건지.... \n아~~~해보세요 아~~~~
hidetext

/////////////////////////////////////////////// 용사 초필살기
expression Epsilon empty
wait 1.5
motion Epsilon happy
motion Haru surprise
name 용사
text ...........
wait 1.0
hidetext

bgcolor (200,200,255)
wait 0.05
bg maobg
wait 0.15

bgcolor (200,200,255)
wait 0.05
bg maobg
wait 0.05
bgcolor (200,200,255)
wait 0.05
bg maobg
wait 0.05

expression Epsilon smile
modelpos Epsilon (-0.75,-1,0) 0.03
bgcolor (220,220,255)
wait 0.01
bg maobg
modelpos Epsilon (-0.68,-1,0) 0.03
bgcolor (220,220,255)
wait 0.01
bg maobg
modelpos Epsilon (-0.60,-1,0) 0.03
bgcolor (220,220,255)
wait 0.01
bg maobg
modelpos Epsilon (-0.51,-1,0) 0.05
bgcolor (220,220,255)
wait 0.01
bg maobg
modelpos Epsilon (-0.42,-1,0) 0.05
bgcolor (220,220,255)
wait 0.01
bg maobg
modelpos Epsilon (-0.30,-1,0) 0.07
bgcolor (220,220,255)
wait 0.01
bg maobg
modelpos Epsilon (-0.15,-1,0) 0.09
bgcolor (220,220,255)
wait 0.01
bg maobg
modelpos Epsilon (0.0,-1,0) 0.10
bgcolor (220,220,255)
wait 0.01
bg maobg
modelpos Epsilon (0.2,-1,0) 0.10

fg
sound DooDoong
bgcolor (255,255,255)
modelhide Epsilon 0
modelhide Haru 0
fgout 2.0

wait 2.5
name 마왕
text (.......분명 미연시 과제라고 들었는데.....)

title
